﻿using System;
using OpenNETCF.MQTT;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SmartLight.Tools
{
    public class MQTT
    {
        public string Broker { get; private set; }

        private readonly MQTTClient clienteMqtt;
        public event EventHandler<string> MensajeRecibido;
        public event EventHandler<string> Deconectado;
        public event EventHandler<string> Conectado;
        public MQTT(string broker, int puerto)
        {
            Broker = broker;
            clienteMqtt = new MQTTClient(broker, puerto);
            clienteMqtt.Connected += ClienteMqtt_Connected;
            clienteMqtt.Disconnected += ClienteMqtt_Disconnected;
            clienteMqtt.MessageReceived += ClienteMqtt_MessageReceived;
            
        }

        private void ClienteMqtt_Connected(object sender, EventArgs e)
        {
            Conectado(this, $"Conectado a {Broker}");
        }

        private void ClienteMqtt_Disconnected(object sender, EventArgs e)
        {
            Deconectado(this, $"Se perdio la conexión al broker {Broker}");
        }

        public void Connect(string clientId, string usermane=null,string password=null )
        {
            clienteMqtt.Connect(clientId, usermane, password);
        }
        private void ClienteMqtt_MessageReceived(string topic, QoS qos, byte[] payload)
        {
            MensajeRecibido(this, System.Text.Encoding.UTF8.GetString(payload));
        }
        public void addSubscription(string tema)
        {
            clienteMqtt.Subscriptions.Add(new Subscription(tema));
        }
        public bool Publicar(string mensaje,string topic)
        {
            if (clienteMqtt.IsConnected)
            {
                clienteMqtt.Publish(topic, mensaje, QoS.FireAndForget, false);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
