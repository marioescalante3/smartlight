﻿using SmartLight.COMMON.Entities;
using SmartLight.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace SmartLight.BIZ
{
    public class GenericManager<T> : IGenericManager<T> where T : BaseDTO
    {
        internal IGenericRepository<T> _repo;
        public GenericManager(IGenericRepository<T> repo) => _repo = repo;
        public string Error => _repo.Error;

        public IEnumerable<T> ObtenerTodos => _repo.Read;

        public T Actualizar(T entidad)
        {
            return _repo.Update(entidad);
        }

        public IEnumerable<T> BuscarPorFechaHoraCreacion(DateTime? inicio, DateTime? fin)
        {
            return _repo.SearchByDateTimeCreate(inicio, fin);
        }

        public T BuscarPorId(string id)
        {
            return _repo.SearchById(id);
        }

        public IEnumerable<T> Consulta(Expression<Func<T, bool>> predicado)
        {
            return _repo.Query(predicado);
        }

        public T Crear(T entidad)
        {
            return _repo.Create(entidad);
        }

        public bool Eliminar(T entidadEliminar)
        {
            return _repo.Delete(entidadEliminar);
        }
    }
}
