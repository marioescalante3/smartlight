﻿using SmartLight.COMMON.Entities;
using SmartLight.DAL.Local.LiteDB;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartLight.BIZ
{
   public  class FactoryManager
    {
        public  DeviceManager DeviceManager { get; private set; }
        public HomeManager HomeManager { get; private set; }
        public RoomManager RoomManager { get; private set; }
        public UserManager UserManager { get; private set; }

        public FactoryManager()
        {
            DeviceManager = new DeviceManager(new GenericRepository<Device>());
            HomeManager = new HomeManager(new GenericRepository<Home>());
            RoomManager = new RoomManager(new GenericRepository<Room>());
            UserManager = new UserManager(new GenericRepository<User>());
        }
    }
}
