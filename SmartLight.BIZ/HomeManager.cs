﻿using SmartLight.COMMON.Entities;
using SmartLight.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartLight.BIZ
{
    public class HomeManager : GenericManager<Home>, IHomeManager
    {
        public HomeManager(IGenericRepository<Home> repo) : base(repo)
        {
        }

        public IEnumerable<Home> SearchByAddress(string address)
        {
            return _repo.Query(home => home.Addrees == address);
        }

        public IEnumerable<Home> SearchByIdUser(string idUser)
        {
            return _repo.Query(home => home.IdUser == idUser);
        }

        public IEnumerable<Home> SearchByName(string name)
        {
            return _repo.Query(home => home.Name.Contains(name));
        }

        public Home SearchBySpecificName(string name)
        {
            return _repo.Query(home => home.Name==name).SingleOrDefault();
        }
    }
}
