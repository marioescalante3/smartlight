﻿using SmartLight.COMMON.Entities;
using SmartLight.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartLight.BIZ
{
    public class RoomManager : GenericManager<Room>, IRoomManager
    {
        public RoomManager(IGenericRepository<Room> repo) : base(repo)
        {
        }

        public IEnumerable<Room> SearchByIdHome(string idHome)
        {
            return _repo.Query(room => room.IdHome == idHome);
        }

        public IEnumerable<Room> SearchByName(string name)
        {
            return _repo.Query(room => room.Name.Contains(name));
        }

        public Room SearchBySpecificName(string name)
        {
            return _repo.Query(room => room.Name==name).SingleOrDefault();
        }
    }
}
