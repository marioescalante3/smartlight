﻿using SmartLight.COMMON.Entities;
using SmartLight.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartLight.BIZ
{
    public class DeviceManager : GenericManager<Device>, IDeviceManager
    {
        public DeviceManager(IGenericRepository<Device> repo) : base(repo)
        {
        }

        public IEnumerable<Device> SearchByIdRoom(string idRoom)
        {
          return  _repo.Query(device => device.IdRoom == idRoom);
        }

        public IEnumerable<Device> SearchByName(string name)
        {
            return _repo.Query(device => device.Name.Contains(name));
        }

        public Device SearchBySpecificName(string name)
        {
            return _repo.Query(device => device.Name==name).SingleOrDefault(device=>device.Name==name);
        }
    }
}
