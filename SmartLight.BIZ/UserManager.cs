﻿using SmartLight.COMMON.Entities;
using SmartLight.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SmartLight.BIZ
{
    public class UserManager : GenericManager<User>, IUserManager
    {
        public UserManager(IGenericRepository<User> repo) : base(repo)
        {
        }

        public User Login(string userName, string password)
        {
            return _repo.Query(user => user.UserName == userName && user.Password == password).SingleOrDefault();
        }

        public IEnumerable<User> SearchByName(string name)
        {
            return _repo.Query(user => user.Name.Contains(name));
        }

        public User SearchBySpecificName(string name)
        {
            return _repo.Query(user => user.Name == name).SingleOrDefault();
        }
    }
}
