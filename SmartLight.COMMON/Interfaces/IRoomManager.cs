﻿using SmartLight.COMMON.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartLight.COMMON.Interfaces
{
   public interface IRoomManager:IGenericManager<Room>
    {
        IEnumerable<Room> SearchByName(string name);
        Room SearchBySpecificName(string name);
        IEnumerable<Room> SearchByIdHome(string idHome);

    }
}
