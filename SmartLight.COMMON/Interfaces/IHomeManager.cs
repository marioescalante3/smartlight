﻿using SmartLight.COMMON.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartLight.COMMON.Interfaces
{
  public  interface IHomeManager:IGenericManager<Home>
    {
        IEnumerable<Home> SearchByName(string name);
        Home SearchBySpecificName(string name);
        IEnumerable<Home> SearchByAddress(string address);
        IEnumerable<Home> SearchByIdUser(string idUser);


    }
}
