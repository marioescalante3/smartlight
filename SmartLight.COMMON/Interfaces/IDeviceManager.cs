﻿using SmartLight.COMMON.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartLight.COMMON.Interfaces
{
   public interface IDeviceManager:IGenericManager<Device>
    {
        IEnumerable<Device> SearchByName(string name);
        Device SearchBySpecificName(string name);
        IEnumerable<Device> SearchByIdRoom(string idRoom);
    }
}
