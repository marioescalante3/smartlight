﻿using SmartLight.COMMON.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SmartLight.COMMON.Interfaces
{
   public interface IUserManager:IGenericManager<User>
    {
        IEnumerable<User> SearchByName(string name);
        User SearchBySpecificName(string name);
        User Login(string userName, string password);
    }
}
