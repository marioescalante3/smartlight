﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartLight.COMMON.Entities
{
   public class BaseDTO
    {
        public string Id { get; set; }
        public DateTime DateTimeCreate { get; set; }
        public string Notes { get; set; }
    }
}
