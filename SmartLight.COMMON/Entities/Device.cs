﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartLight.COMMON.Entities
{
   public class Device:BaseDTO
    {
        public string Name { get; set; }
        public string IdRoom { get; set; }
        public string CodeSync { get; set; }
    }
}
