﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartLight.COMMON.Entities
{
  public  class User:BaseDTO
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
    }
}
