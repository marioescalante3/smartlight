﻿using SmartLight.COMMON.Entities;
using SmartLight.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmartLight.Page
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Dashboard : ContentPage
    {
        IHomeManager _homeManager;
        public Dashboard()
        {
            InitializeComponent();
            _homeManager = Tools.FactoryManager.factoryManager.HomeManager;
            titulo.Text = $"Casas de {Tools.VariablesComunes._usuarioLogeado.Name} {Tools.VariablesComunes._usuarioLogeado.LastName}";
            ActualizarDatos();
        }

        [Obsolete]
        private async void NewHome(object sender, EventArgs e)
        {
            string nombre = await DisplayPromptAsync("SmartLight", "Escriba el nombre de la nueva casa", "Ok", "Cancelar", "name");
            string direccion = await DisplayPromptAsync("SmartLight", "Escriba la direccion de la nueva casa", "Ok", "Cancelar", "address");
            if (string.IsNullOrEmpty(direccion) || string.IsNullOrEmpty(nombre))
            {
                await DisplayAlert("Smartlight", "verfifque datos para poder crear una casa", "Ok");
                return;
            }
            if (_homeManager.Crear(new COMMON.Entities.Home() { Addrees = direccion, Name = nombre, IdUser = Tools.VariablesComunes._usuarioLogeado.Id }) is Home result)
            {
                ActualizarDatos();
            }
            else
            {
                await DisplayAlert("Smartlight", $"Error al crear la nueva casa {_homeManager.Error}", "Ok");
            }
        }

        private void DeleteHome(object sender, EventArgs e)
        {
            Home homeSelect= ((sender as MenuItem).CommandParameter as Home);
            if (_homeManager.Eliminar(homeSelect))
            {
                DisplayAlert("SmartLight", "La casa se elimino correctamente", "Ok");
                ActualizarDatos();
            }
            else
            {
                DisplayAlert("SmartLight", $"Error al eliminar la casa {_homeManager.Error}", "Ok");
            }
        }

        private async void EditHome(object sender, EventArgs e)
        {
            string nombreNuevo = await DisplayPromptAsync("SmartLight", "Escriba el nombre de la nueva casa", "Ok", "Cancelar", "deje en blanco para no modificar nombre");
            string direccionNueva = await DisplayPromptAsync("SmartLight", "Escriba la direccion de la nueva casa", "Ok", "Cancelar", "deje en blanco para no modificar direccion");
            Home homeSelect = ((sender as MenuItem).CommandParameter as Home);
            homeSelect.Name = string.IsNullOrEmpty(nombreNuevo)? homeSelect.Name:nombreNuevo;
            homeSelect.Addrees = string.IsNullOrEmpty(direccionNueva) ? homeSelect.Addrees:direccionNueva;
            if (_homeManager.Actualizar(homeSelect) is Home result)
            {
                ActualizarDatos();
            }
            else
            {
                await DisplayAlert("Smartlight", $"Error al crear la nueva casa {_homeManager.Error}", "Ok");
            }
            ActualizarDatos();
        }

        private void ActualizarDatos()
        {
            datosHome.ItemsSource = null;
            datosHome.ItemsSource = _homeManager.SearchByIdUser(Tools.VariablesComunes._usuarioLogeado.Id);
        }

        private void datosHome_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Navigation.PushAsync(new Habitaciones(e.SelectedItem as Home), true);
        }
    }
}