﻿using SmartLight.COMMON.Entities;
using SmartLight.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmartLight.Page
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Habitaciones : ContentPage
    {
        IRoomManager _roomManager;
        Home _home;
        public Habitaciones(Home home)
        {
            InitializeComponent();
            _home = home;
            _roomManager= Tools.FactoryManager.factoryManager.RoomManager;
            ActualizarLista();
        }

        private async void NewRoom(object sender, EventArgs e)
        {
            string nombre = await DisplayPromptAsync("SmartLight", "Escriba el nombre de la nueva habitacion", "Ok", "Cancelar", "name");
            if (string.IsNullOrEmpty(nombre))
            {
               await DisplayAlert("SmartLight", "No se ha dado un nombre a la nueva habitacion, no se puede crear una habitacion sin nombre", "Ok");
            }
            if (_roomManager.Crear(new Room() { Name=nombre, IdHome=_home.Id }) is Room roomResult)
            {
                await DisplayAlert("SmartLight", "Habitacion creada correctamente", "Ok");
            }
            else
            {
                await DisplayAlert("SmartLight", $"Error al crear habitacion {_roomManager.Error}", "Ok");
            }
            ActualizarLista();
        }

        private async void DeleteRoom(object sender, EventArgs e)
        {
            Room roomEdit = (sender as MenuItem).CommandParameter as Room;

            if (_roomManager.Eliminar(roomEdit))
            {
                await DisplayAlert("SmartLight", "Habitacion eliminada correctamente", "Ok");
            }
            else
            {
                await DisplayAlert("SmartLight", $"Error al eliminar habitacion {_roomManager.Error}", "Ok");
            }
            ActualizarLista();
        }
        private async void EditRoom(object sender, EventArgs e)
        {
            Room roomEdit = (sender as MenuItem).CommandParameter as Room;
            string nombre = await DisplayPromptAsync("SmartLight", "Escriba el nombre de la nueva habitacion", "Ok", "Cancelar", "name",-1,null,roomEdit.Name);
            if (string.IsNullOrEmpty(nombre))
            {
                await DisplayAlert("SmartLight", "No se ha dado un nombre a la nueva habitacion, no se puede editar una habitacion sin nombre", "Ok");
            }
            roomEdit.Name = nombre;
            if (_roomManager.Actualizar(roomEdit) is Room roomResult)
            {
                await DisplayAlert("SmartLight", "Habitacion editada correctamente", "Ok");
            }
            else
            {
                await DisplayAlert("SmartLight", $"Error al editar habitacion {_roomManager.Error}", "Ok");
            }
            ActualizarLista();
        }

        private void ActualizarLista()
        {
            Datos.ItemsSource = null;
            Datos.ItemsSource = _roomManager.SearchByIdHome(_home.Id);
        }

        private void ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Room roomSelect = e.SelectedItem as Room;
            Navigation.PushAsync(new DevicePage(roomSelect), true);
        }
    }
}