﻿using SmartLight.COMMON.Entities;
using SmartLight.COMMON.Interfaces;
using SmartLight.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmartLight.Page
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        IUserManager _userManager;
        public Login()
        {
            InitializeComponent();
            _userManager = Tools.FactoryManager.factoryManager.UserManager;
        }

        private void BtnLogin_Clicked(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(userName.Text) || string.IsNullOrEmpty(password.Text))
            {
                DisplayAlert("SmartLight", "Contraseña y/o nombre de usuario no se ha escrito", "Ok");
                return;
            }
            if (_userManager.Login(userName.Text, password.Text) is User userLogged)
            {
                VariablesComunes._usuarioLogeado = userLogged;
                Navigation.PushAsync(new Dashboard(), true);
            }
            else
            {
                DisplayAlert("SmartLight", "Contraseña y/o nombre de usuario incorrectos", "Ok");
            }
        }
    }
}