﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SmartLight.COMMON.Interfaces;
using SmartLight.COMMON.Entities;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmartLight.Page
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class signin : ContentPage
    {
        IUserManager _userManager;
        public signin()
        {
            InitializeComponent();
            _userManager = SmartLight.Tools.FactoryManager.factoryManager.UserManager;
        }

        private void BtnsignIn_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(username.Text) || string.IsNullOrEmpty(name.Text) || string.IsNullOrEmpty(lastname.Text) || string.IsNullOrEmpty(password.Text) || string.IsNullOrEmpty(passowrd2.Text))
            {
                DisplayAlert("SmartLight", "favor de verificar datos", "ok");
                return;
            }
            if (passowrd2.Text!=passowrd2.Text)
            {
                DisplayAlert("SmartLight", "Las contraseñas no coinciden", "Ok");
                return;
            }
            if (_userManager.Crear(new COMMON.Entities.User() { LastName=lastname.Text, Name=name.Text, Password=password.Text, UserName=username.Text }) is User userCreate)
            {
                DisplayAlert("SmartLight", "Usuario creado correctamente, inicia sesion con tu nombre de usuario y contraseña", "Ok");
            }
            else
            {
                DisplayAlert("SmartLight", $"error al crear usuario {_userManager.Error}", "Ok");
            }
        }
    }
}