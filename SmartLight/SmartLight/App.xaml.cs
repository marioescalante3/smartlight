﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SmartLight
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new Page.Inicio();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
