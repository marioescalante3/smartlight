﻿using LiteDB;
using SmartLight.COMMON.Entities;
using SmartLight.COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SmartLight.DAL.Local.LiteDB
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        string dbName;
        public GenericRepository()
        {
            dbName = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + @"\\SmartLight.db";
        }
        //  private LiteDatabase db => new LiteDatabase(new ConnectionString() { Filename = dbName });
        public IEnumerable<T> Read
        {
            get
            {
                List<T> datos;
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    datos = db.GetCollection<T>(typeof(T).Name).FindAll().ToList();
                }

                // select * from Productos
                return datos;
            }
        }

        public string Error { get; private set; }

        public T Create(T entity)
        {
            try
            {
                entity.Id = Guid.NewGuid().ToString();
                entity.DateTimeCreate = DateTime.Now;
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    db.GetCollection<T>(typeof(T).Name).Insert(entity);
                }

                return entity;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
        public T Update(T entity)
        {
            try
            {
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    db.GetCollection<T>(typeof(T).Name).Update(entity);
                }

                return entity;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return null;
            }
        }
        public bool Delete(T entityToDelete)
        {
            try
            {
                bool result = false;
                using (var db = new LiteDatabase(new ConnectionString() { Filename = dbName }))
                {
                    result = db.GetCollection<T>(typeof(T).Name).Delete(entityToDelete.Id);
                }
                return result;
            }
            catch (Exception ex)
            {
                Error = ex.Message;
                return false;
            }
        }
        public T SearchById(string id)
        {
            //return Query(e => e.Id == id).SingleOrDefault(); hace lo mismo que el metodo de abajo
            return Read.Where(g => g.Id == id).SingleOrDefault();
        }
        public IEnumerable<T> Query(Expression<Func<T, bool>> query)
        {
            return Read.Where(query.Compile()).ToList();
        }

        public IEnumerable<T> SearchByDateTimeCreate(DateTime? inicio, DateTime? fin)
        {
            if (inicio != null && fin != null)
            {
                return Query(entity => entity.DateTimeCreate >= inicio && entity.DateTimeCreate <= fin.Value.AddDays(1));
            }
            if (inicio != null)
            {
                return Query(entity => entity.DateTimeCreate >= inicio);
            }
            if (fin != null)
            {
                return Query(entity => entity.DateTimeCreate <= fin.Value.AddDays(1));
            }
            Error = "No se pudo obtener datos, revise fechas";
            return null;
        }
    }
}
